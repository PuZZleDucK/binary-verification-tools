
# Building a verification toolkit without a software department - Part 2

## Creating binary disks and acquiring images

Another tool available with verifiable source is "dd" and it's younger cousins "dc3dd" and "dcfldd". These tools enable us to independently take an image of a PSD so we can compare it with a known reference binary image. In most cases this image will be the same image that a manufacturers tool will extract but sometimes this technique will also show additional work performed by the manufacturers tooling.

Before beginning using dd and it's associated tools it is a good idea to create backups of all essential partitions on the machine performing the transfer prior to any other copies. This will allow us to restore the system to a usable state in case we accidentally misuse dd. dd is the chainsaw of file transfers and chainsaws require careful handling and proper precautions. The simplest way to discover which drives to backup is to use the Gnome Disk Utility (aka: "palimpsest" on the command line) or equivalent disk manager from any modern Linux distribution. Take note of the device names for each main hard drive, these will usually be of the form "/dev/sd\*", "/dev/hd\*" or "/dev/mmcblk\*". The command below assumes that the maximum PSD size likely to be used is 4GB, if for example an 8GB medium is going to be used then the "count=" parameter should be doubled (or scaled appropriately). Replace "\<disk-id\>" in the command below with these device names to create emergency backup images.

~~~~
sudo dd if=/dev/<disk-id> of=~/backups/<disk-id>.img bs=1MB count=4096
~~~~

Then if dd is accidentally misused to overwrite an essential disk it can be restored by restoring the backup as soon as possible:

~~~~
sudo dd if=~/backups/<disk-id>.img of=/dev/<disk-id> bs=1MB
~~~~

Now with precautions out of the way we can get to the task at hand. Again we can use the Gnome Disk Utility to discover the device we will be using to read the manufacturers PSD. Open the Disk Utility and observe the device id of the disk that appears when the device writer is plugged into the system ('lsusb' and 'df' can also be useful tools for identifying devices). Lets assume that we have a compact flash card with an image which we want to compare to a reference binary, and we've discovered it's device /dev/sde. We can use the following command to pull a binary image of the compact flash card.

~~~~
sudo dd if=/dev/sde of=~/images/image-name.img bs=1MB
~~~~

The "if=" parameter specifies the low level device we want to read and "of=" specifies the image file we will create on the verification machine. The "bs=1MB" is required to speed up the process as dd often uses a very small default blocksize and transferring large images can be time consuming. Larger blocksizes can speed up the process more on some devices, but be warned that pushing the limits of speed can result in read/write errors so stick with 1MB or test the limits of the verification machine and reading hardware thoroughly. Additionally the "dc3dd" and "dcfldd" tools allow a simpler command and provide feedback on the status of the read if available. Additional benefits include not having to specify block size or utilize sudo. You'll notice the "if=" and "of=" parameters are identical and we have been able to drop "bs=" and "sudo" parts of the command leaving us to simply specify the source and destination.

~~~~
dc3dd if=/dev/sde of=~/images/image-name.img
~~~~

We can now use the diff tool to verify we have the same image as the reference binary. Be wary however of false negatives if a binary does not exactly match the dd image. Closer inspection should be undertaken in the case where a diff does not match as this may be due to padding, corruption of the medium or issues caused during transfer which is outside the scope of this document to detail.

~~~~
diff --report-identical-files <referance-image>.img image-name.img
~~~~

Hopefully you'll find these tools useful independent sources of common crypto and disk functions. Next up in the series We'll look into combining signature files and analyzing some of those pesky discrepancies that can occur reading and writing images and signatures.

## Additional dd resources:

- [Dc3dd Summary](http://forensicswiki.org/wiki/Dc3dd)
- [Dcfldd Summary](http://forensicswiki.org/wiki/Dcfldd)
- [Suse DFIR Portal](https://en.opensuse.org/Portal:Digital_forensics_and_incident_response)

