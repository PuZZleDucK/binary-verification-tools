# Building a verification toolkit without a software department - Part 1

## Binary image verification

For larger companies and manufacturers who already heavily invest in R&D and software development there is a low barrier to produce in-house binary image writing and verification tools. However for the new entrant into the industry and entities who do not invest in producing their own software the prospect of commissioning custom software could seem a daunting clallenge. However many may not be aware that the state of the art in cryptogragraphic verification is right at our fingertips in an open source format which is itself verifiable and inspectable.

OpenSSL provides a plethora of hash based verification methods and with very few exceptions (and combining it with crc32) can be used to verify the binary images for almost all jurisdictions. If you are part of a regulatory body considering a new hash technique, ensuring it is avaliable in OpenSSL makes sure you'll always have an independant, verifiable reference implementation that is easy for manufacturers to verify their implementations against and easy for testlabs to start utilizing.

The following bash functions can be put into your bashrc file to allow very convenient verification of binaries:

~~~~bash
# Image Verification Functions
function hmac-sha1 () { date > hmac-sha1.txt; openssl dgst -sha1 -mac HMAC -macopt hexkey:0000 $@ | tee -a hmac-sha1.txt; }

function sha1 () { date > sha1.txt; openssl dgst -sha1 $@ | tee -a sha1.txt; }

function crc () { date > crc.txt; crc32 $@ | tee -a crc.txt; }
~~~~

so now we can just run the commands (replacing <file-name> with the aquired image):

~~~~script
sha1 <file-name>
hmac-sha1 <file-name>
crc <file-name>
~~~~

and we get the ouput files below containing timestams and the cryptographic digest of the supplied file:

~~~~terminal
sha1.txt
hmac-sha1.txt
crc.txt
~~~~

This give us timestamped logs of the digest values that allow us to verify binary images for the majority of jurisdictions around the world. Next in the series we'll look into tools for aquiring the binary images from a given device.

## OpenSSL Resources
[OpenSSL Command](https://www.openssl.org/docs/apps/openssl.html)
[Digest Function](https://www.openssl.org/docs/apps/sha.html)
